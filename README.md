# Mini



## URL Minifier
This is the test task.

Workflow:
User came, sign up, insert their long url and get compact url.
By following the generated compact URL the service redirects users to the source/destination URL.

User can also set the Link expiration time.
User can create a bunch of Links and easily manage them.
In addition, service gathers some basic statistics about the Links follows/redirections

Tech stack:
XAMPP
Apache    2.4.43
MariaDB   10.4.13
PHP       7.4.7
Symfony   5.4
Bootstrap 5.2
