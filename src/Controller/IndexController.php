<?php

namespace App\Controller;

use App\Entity\Link;
use App\Form\MinifyUrlFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * This is the root route handler
 */
class IndexController extends AbstractController {
	public function index(): Response {
		// If user is authenticated - show minify url form
		if ($this->getUser()) {
			$link = new Link();
			$form = $this->createForm(MinifyUrlFormType::class, $link, [
				'action' => $this->generateUrl('link_create'),
				'method' => 'POST',
			]);

			return $this->renderForm('index/index.html.twig', [
				'minifyUrlForm' => $form
			]);
		} else {
			return $this->render('index/index.html.twig');
		}
	}
}
