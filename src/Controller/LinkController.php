<?php

namespace App\Controller;

use App\Entity\Link;
use App\Form\MinifyUrlFormType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\ByteString;

/**
 * This controller is responsible for Link browsing, viewing, generating and handling
 */
class LinkController extends AbstractController {
	private $security;

	public function __construct(Security $security) {
		$this->security = $security;
	}

	/**
	 * Browse user's links
	 */
	public function browse(): Response {
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

		return $this->render('link/browse.html.twig', [
			'links' => $this->security->getUser()->getLinks()
		]);
	}

	/**
	 * View link's information and statistics
	 */
	public function read(ManagerRegistry $doctrine, string $code): Response {
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

		$repository = $doctrine->getRepository(Link::class);
		$link = $repository->findOneBy(['code' => $code]);

		return $this->render('link/read.html.twig', [
			'link' => $link
		]);
	}

	/**
	 * Loads Create Link Form
	 */
	public function add(): Response {
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

		$link = new Link();
		$form = $this->createForm(MinifyUrlFormType::class, $link, [
			'action' => $this->generateUrl('link_create'),
			'method' => 'POST',
		]);

		return $this->renderForm('link/add.html.twig', [
			'minifyUrlForm' => $form
		]);
	}

	/**
	 * Handles Link creating
	 */
	public function create(Request $request, EntityManagerInterface $entityManager): Response {
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

		$link = new Link();
		$form = $this->createForm(MinifyUrlFormType::class, $link, [
			'action' => $this->generateUrl('link_create'),
			'method' => 'POST',
		]);

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$link->setUser($this->security->getUser());
			$link->setTtl(time() + $form->get('ttl')->getData());
			$link->setCode(ByteString::fromRandom(10)->toString());
			$link->setFollowsCount(0);

			$entityManager->persist($link);
			$entityManager->flush();

			$this->addFlash('success', 'Link created!');
			return new RedirectResponse($this->generateUrl('link_browse'));
		} else {
			return $this->renderForm('link/add.html.twig', [
				'minifyUrlForm' => $form
			]);
		}
	}

	/**
	 * Handles Link deleting
	 */
	public function delete(ManagerRegistry $doctrine, string $code): Response {
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

		$repository = $doctrine->getRepository(Link::class);
		$link = $repository->findOneBy(['code' => $code]);
		if ($link && $link->getUser() == $this->security->getUser()) {
			$this->addFlash('success', 'Link removed!');

			$entityManager = $doctrine->getManager();
			$entityManager->remove($link);
			$entityManager->flush();

			return new RedirectResponse($this->generateUrl('link_browse'));
		} else {
			throw $this->createNotFoundException('Not found');
		}
	}

	/**
	 * Handles Link redirecting and Link follows counting
	 */
	public function follow(ManagerRegistry $doctrine, string $code): Response {
		$entityManager = $doctrine->getManager();
		$link = $entityManager->getRepository(Link::class)->findOneBy(['code' => $code]);
		// Check if link exists and if it is still valid
		if ($link && $link->getTtl() > time()) {
			// Increment follows count
			$link->setFollowsCount($link->getFollowsCount() + 1);
			$entityManager->flush();

			// Collect some other data for statistics like useragent, locations(countries), referrers, daytime etc.

			return $this->redirect($link->getDestination());
		} else {
			throw $this->createNotFoundException('Not found');
		}
	}
}
