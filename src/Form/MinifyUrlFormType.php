<?php

namespace App\Form;

use App\Entity\Link;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Url;

class MinifyUrlFormType extends AbstractType {
	public function buildForm(FormBuilderInterface $builder, array $options): void {
		$builder
			->add('destination', UrlType::class, [
				'constraints' => [
					new NotBlank([
						'message' => 'Please enter an Url'
					]),
					new Url([
						'message' => 'Please enter a valid Url'
					]),
					new Length([
						'max'        => 1024,
						'maxMessage' => 'Your url should be maximum of {{ limit }} characters'
					]),
				],
			])
			->add('ttl', IntegerType::class, [
				'mapped'      => false,
				'constraints' => [
					new NotBlank([
						'message' => 'Please enter a Time to live'
					]),
					new Range([
						'min'               => 0,
						'max'               => 2592000,
						'notInRangeMessage' => 'Time to live must be between {{ min }} and {{ max }} seconds'
					])
				]
			]);
	}

	public function configureOptions(OptionsResolver $resolver): void {
		$resolver->setDefaults([
			'data_class' => Link::class,
		]);
	}
}
