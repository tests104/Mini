<?php

namespace App\Entity;

use App\Repository\LinkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LinkRepository::class)
 * @ORM\Table(name="links")
 */
class Link {
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="links")
	 * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id", nullable=false)
	 */
	private $user;

	/**
	 * @ORM\Column(type="string", length=16)
	 */
	private $code;

	/**
	 * @ORM\Column(type="string", length=1024)
	 */
	private $destination;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $ttl;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $follows_count;

	public function getId(): ?int {
		return $this->id;
	}

	public function getUser(): ?User {
		return $this->user;
	}

	public function setUser(?User $user): self {
		$this->user = $user;

		return $this;
	}

	public function getCode(): ?string {
		return $this->code;
	}

	public function setCode(string $code): self {
		$this->code = $code;

		return $this;
	}

	public function getDestination(): ?string {
		return $this->destination;
	}

	public function setDestination(string $destination): self {
		$this->destination = $destination;

		return $this;
	}

	public function getTtl(): ?int {
		return $this->ttl;
	}

	public function setTtl(int $ttl): self {
		$this->ttl = $ttl;

		return $this;
	}

	public function getFollowsCount(): ?int {
		return $this->follows_count;
	}

	public function setFollowsCount(int $follows_count): self {
		$this->follows_count = $follows_count;

		return $this;
	}
}
